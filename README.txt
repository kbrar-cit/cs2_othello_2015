Team BSquad README.

Solo project by Kabir Brar.

The main improvements I made to this code were to implement minimax recursively
and add alpha-beta pruning. Originally, I had written minimax to work 
iteratively with a hard-depth of 2 ply, which did work reasonably well against
SimplePlayer. It took me a while to get alpha-beta pruning working properly,
so I wasn't able to implement iterative deepening as well. An interesting
note is that alpha-beta pruning (on its own) seems to only beat
ConstantTimePlayer if it goes to a depth of at least 5 ply. It makes sense
that alpha-beta pruning works at this level, since ConstantTimePlayer only
picks moves by performing calculations on the current moves available to it,
and does not look ahead to see how these moves will affect its long-term
performance. All of this being said, I don't think that my AI would perform
at well in a tournament since it is close to barebones at the moment, and
it only has one "real" strategy: alpha-beta pruning to a fixed depth.
