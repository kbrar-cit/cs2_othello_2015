#ifndef __PLAYER_H__
#define __PLAYER_H__

#include <iostream>
#include "common.h"
#include "board.h"
#include <vector>

using namespace std;

class Player {

public:
    Player(Side side);
    ~Player();

    void setBoard(Board *newBoardState);

    std::vector<MoveTreeNode*> makeChildrenNodes(MoveTreeNode * parent,
                                                 Board * startBoard,
                                                 Side side);
    int alphaBetaHelper(Board * currentBoard,
                        Side  thisSide,
                        MoveTreeNode * node, 
                        int currentDepth, 
                        int inheritedAlpha, 
                        int inheritedBeta);

    Move *doAlphaBeta();
    Move *doBasicMinimax();
    Move *findNextBestMove();
    Move *doMove(Move *opponentsMove, int msLeft);

    // Flag to tell if the player is running within the test_minimax context
    bool testingMinimax;

    bool doingAlphaBeta;


private:
    Board * boardState;
    Board * testBoard;
    Side mySide;
    Side opponentSide;
    int depth;

    int calculateScore(Move * possibleMove, Board * tempBoard, Side side);
};

#endif
