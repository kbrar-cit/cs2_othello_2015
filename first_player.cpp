#include "player.h"
#include <vector>
#include <ctime>
#include <cstdlib>

/*
 * Constructor for the player; initialize everything here. The side your AI is
 * on (BLACK or WHITE) is passed in as "side". The constructor must finish 
 * within 30 seconds.
 */
Player::Player(Side side) {
    // Will be set to true in test_minimax.cpp.
    testingMinimax = false;
    boardState = new Board();
    mySide = side;
    if(mySide == WHITE){
        opponentSide = BLACK;
    }
    else{
        opponentSide = WHITE;
    }
    srand(time(NULL));
}

/*
 * Destructor for the player.
 */
Player::~Player() {
}

/*
 * Compute the next move given the opponent's last move. Your AI is
 * expected to keep track of the board on its own. If this is the first move,
 * or if the opponent passed on the last move, then opponentsMove will be NULL.
 *
 * msLeft represents the time your AI has left for the total game, in
 * milliseconds. doMove() must take no longer than msLeft, or your AI will
 * be disqualified! An msLeft value of -1 indicates no time limit.
 *
 * The move returned must be legal; if there are no valid moves for your side,
 * return NULL.
 */
Move *Player::doMove(Move *opponentsMove, int msLeft) {
    boardState->doMove(opponentsMove, opponentSide);
    if(!boardState->hasMoves(mySide)){
        std::cerr << "no moves left for BSquad." << std::endl;
        return NULL;
    }
    std::vector<Move*> possibleMoves;
    Move * tempMove = new Move(0, 0);
    for (int i = 0; i < 8; i++) {
        for (int j = 0; j < 8; j++) {
            tempMove->setX(i);
            tempMove->setY(j);
            if (boardState->checkMove(tempMove, mySide)){
                 possibleMoves.push_back(new Move(i, j));
            }
        }
    }
    delete tempMove;
    int moveIndex = rand() % possibleMoves.size();
    std::cerr << "chosen move for BSquad: x = " 
              << possibleMoves[moveIndex]->getX() 
              << " y = " 
              << possibleMoves[moveIndex]->getY() << endl;
    boardState->doMove(possibleMoves[moveIndex], mySide);
    return possibleMoves[moveIndex];
}
