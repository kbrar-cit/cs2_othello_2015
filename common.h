#ifndef __COMMON_H__
#define __COMMON_H__

#include <vector>
#include <limits>

enum Side { 
    WHITE, BLACK
};

class Move {
   
public:
    int x, y;
    Move(int x, int y) {
        this->x = x;
        this->y = y;        
    }
    ~Move() {}

    int getX() { return x; }
    int getY() { return y; }

    void setX(int x) { this->x = x; }
    void setY(int y) { this->y = y; }
};

class MoveTreeNode {

private:
    MoveTreeNode * parent;
    Move * myMove;
    std::vector<MoveTreeNode*> childrenNodes;
    int score;
    int alpha;
    int beta;

public:
    MoveTreeNode(MoveTreeNode * parent, Move * thisMove, int thisScore) {
        this->parent = parent;
        myMove = thisMove;
        score = thisScore;
        alpha = std::numeric_limits<int>::min();
        beta = std::numeric_limits<int>::max();
    }

    ~MoveTreeNode() {
//        delete parent;
//        delete myMove;
    }

    MoveTreeNode * getParent() { return parent; }
    Move * getMove() { return myMove; }
    std::vector<MoveTreeNode*> getChildren() { return childrenNodes; }
    int getScore() { return score; }
    int getAlpha() { return alpha; }
    int getBeta() { return beta; }

    void setMove(Move * m) { myMove = m; }
    void setChildren(std::vector<MoveTreeNode*> c) {
        childrenNodes = c;
    }
    void setScore(int x) { score = x; }   
    void setAlpha(int x) { alpha = x; }
    void setBeta(int x) { beta = x; }

};

#endif
