#include "player.h"
#include <vector>
#include <ctime>
#include <cstdlib>
#include <limits>
#include <algorithm>

/*
 * Constructor for the player; initialize everything here. The side your AI is
 * on (BLACK or WHITE) is passed in as "side". The constructor must finish 
 * within 30 seconds.
 */
Player::Player(Side side) {
    // Will be set to true in test_minimax.cpp.
    // TODO: NOTE TO SELF -- CHANGE THIS CONSTRUCTOR FLAG AS NEEDED
    testingMinimax = true;
    doingAlphaBeta = true;
    boardState = new Board();
    testBoard = new Board();
    mySide = side;
    depth = 5;
    if(mySide == WHITE){
        opponentSide = BLACK;
    }
    else{
        opponentSide = WHITE;
    }
    srand(time(NULL));
}

/*
 * Destructor for the player.
 */
Player::~Player() {
}

void Player::setBoard(Board *newBoardState)
{
    delete boardState;
    delete testBoard;
    boardState = newBoardState;
    testBoard = boardState->copy();
}

std::vector<MoveTreeNode*> Player::makeChildrenNodes(MoveTreeNode * parent, 
                                                     Board * startBoard, 
                                                     Side side)
{
    std::vector<MoveTreeNode*> possibleMoves;
    Move *tempMove = new Move(0, 0);
    for(int i = 0; i < 8; i++){
        for(int j = 0; j < 8; j++){
            tempMove->setX(i);
            tempMove->setY(j);
            if(startBoard->checkMove(tempMove, side)){
                possibleMoves.push_back(new MoveTreeNode(parent, new Move(i, j), 0));
            }
        }
    }
    return possibleMoves;
}

int Player::alphaBetaHelper(Board * currentBoard,
                            Side thisSide,
                            MoveTreeNode * node, 
                            int currentDepth, 
                            int inheritedAlpha, 
                            int inheritedBeta){
    if(currentDepth == depth){
        node->setAlpha(calculateScore(node->getMove(), currentBoard->copy(), thisSide));
        node->setBeta(node->getAlpha());
        delete currentBoard;
        return node->getAlpha();
    }
    std::vector<MoveTreeNode*> children;
    Board * cb = currentBoard->copy();
    cb->doMove(node->getMove(), thisSide);
    if(thisSide == mySide){
        children = makeChildrenNodes(node, cb, opponentSide);
    }
    else{
        children = makeChildrenNodes(node, cb, mySide);
    }
    if(children.size() == 0){
//        std::cerr << "leaf!\n";
        node->setAlpha(calculateScore(node->getMove(), currentBoard->copy(), thisSide));
        node->setBeta(node->getAlpha());
        delete currentBoard;
        return node->getAlpha();
    }
    currentBoard->doMove(node->getMove(), thisSide);
    node->setChildren(children);
    node->setAlpha(inheritedAlpha);
    node->setBeta(inheritedBeta);

    if(thisSide == mySide){ // my move (maximizer)
        int tempMax = std::numeric_limits<int>::min();
        for(unsigned int i = 0; i < children.size(); i++){
            tempMax = alphaBetaHelper(currentBoard->copy(),
                                      opponentSide,
                                      children[i],
                                      currentDepth + 1,
                                      node->getAlpha(),
                                      node->getBeta());
            node->setAlpha(std::max(node->getAlpha(), tempMax));
            node->setBeta(inheritedBeta);
            if(node->getAlpha() >= node->getBeta()){// inheritedBeta){
                delete currentBoard;
                return node->getAlpha(); // cutoff condition
            }
        }
        delete currentBoard;
        return node->getAlpha();
    }
    else{
        int tempMin = std::numeric_limits<int>::max();
        for(unsigned int i = 0; i < children.size(); i++){
            tempMin = alphaBetaHelper(currentBoard->copy(),
                                      mySide,
                                      children[i],
                                      currentDepth + 1,
                                      node->getAlpha(),
                                      node->getBeta());
            node->setAlpha(inheritedAlpha);
            node->setBeta(std::min(node->getBeta(), tempMin));
            if(node->getAlpha() >= node->getBeta()){// inheritedAlpha >= node->getBeta()){
                delete currentBoard;
                return node->getBeta();
            }
        }
        delete currentBoard;
        return node->getBeta();
    }
}

Move *Player::doAlphaBeta()
{
    if(!boardState->hasMoves(mySide)){
        return NULL;
    }
    // find my possible moves:
    Move *tempMove = new Move(0, 0);
    std::vector<MoveTreeNode*> possibleMoves; // guaranteed to have size >= 1
    for(int i = 0; i < 8; i++){
        for(int j = 0; j < 8; j++){
            tempMove->setX(i);
            tempMove->setY(j);
            if(boardState->checkMove(tempMove, mySide)){
                possibleMoves.push_back(new MoveTreeNode(NULL, new Move(i, j), 0));
            }
        }
    }
    
    int ret = std::numeric_limits<int>::max();
    int index = 0;
    for(unsigned int i = 0; i < possibleMoves.size(); i++){
        int temp = alphaBetaHelper(testBoard->copy(), 
                                   mySide,
                                   possibleMoves[i],
                                   0,
                                   std::numeric_limits<int>::min(),
                                   std::numeric_limits<int>::max());
        if(temp < ret){
            ret = temp;
            index = i;
        }
    }
    delete tempMove;

    boardState->doMove(possibleMoves[index]->getMove(), mySide);
    testBoard->doMove(possibleMoves[index]->getMove(), mySide);

    return possibleMoves[index]->getMove();
}
/**
 * If there are possible moves (checked first), this returns this player's
 * next move based on the minimax algorithm with a depth of 2.
**/
Move *Player::doBasicMinimax()
{
    // 2 ply depth (at least for now)
    if(!boardState->hasMoves(mySide)){
        return NULL;
    }

    // find my possible moves:
    Move *tempMove = new Move(0, 0);
    std::vector<Move*> possibleMoves; // guaranteed to have size >= 1
    for(int i = 0; i < 8; i++){
        for(int j = 0; j < 8; j++){
            tempMove->setX(i);
            tempMove->setY(j);
            if(boardState->checkMove(tempMove, mySide)){
                possibleMoves.push_back(new Move(i, j));
            }
        }
    }
    Board * newTestBoard = NULL;
    // now, check the opponent's possible responses:
    int n = possibleMoves.size();
    int * worstCaseScores = new int[n];
    for(unsigned int it = 0; it < possibleMoves.size(); it++){
        newTestBoard = testBoard->copy();
        newTestBoard->doMove(possibleMoves[it], mySide);
        worstCaseScores[it] = 1000; // default high score to be thwarted
        for(int i = 0; i < 8; i++){
            for(int j = 0; j < 8; j++){
                tempMove->setX(i);
                tempMove->setY(j);
                if(newTestBoard->checkMove(tempMove, opponentSide)){
                    Board * temp = newTestBoard->copy();
                    temp->doMove(tempMove, opponentSide);
                    int score;
                    if(mySide == WHITE){
                        score = temp->countWhite() - temp->countBlack();
                    }
                    else{
                        score = temp->countBlack() - temp->countWhite();
                    }

                    // find choice that leads to worst possible outcome:
                    if(score < worstCaseScores[it]){
                        worstCaseScores[it] = score;
                    }
                    delete temp;
                }
            }
        }
    }

    // now determine which decision is the least worst (in the worst case):
    int bestMoveIndex = 0;
    for(unsigned int i = 0; i < possibleMoves.size(); i++){
        if(worstCaseScores[i] > worstCaseScores[bestMoveIndex]){
            bestMoveIndex = i;
        }
    }
    delete newTestBoard;
    delete tempMove;

    boardState->doMove(possibleMoves[bestMoveIndex], mySide);
    testBoard->doMove(possibleMoves[bestMoveIndex], mySide);

    return possibleMoves[bestMoveIndex];
}

/*
 * Compute the next move given the opponent's last move. Your AI is
 * expected to keep track of the board on its own. If this is the first move,
 * or if the opponent passed on the last move, then opponentsMove will be NULL.
 *
 * msLeft represents the time your AI has left for the total game, in
 * milliseconds. doMove() must take no longer than msLeft, or your AI will
 * be disqualified! An msLeft value of -1 indicates no time limit.
 *
 * The move returned must be legal; if there are no valid moves for your side,
 * return NULL.
 */
Move *Player::doMove(Move *opponentsMove, int msLeft) {
    boardState->doMove(opponentsMove, opponentSide);
    testBoard->doMove(opponentsMove, opponentSide);

    if(!boardState->hasMoves(mySide)){
//        std::cerr << "no moves left for BSquad." << std::endl;
        return NULL;
    }

    if(doingAlphaBeta){
//        std::cerr << "doing alpha beta!\n";
        return doAlphaBeta();
    }

    if(testingMinimax){
//        std::cerr << "doing minimax!\n";
        return doBasicMinimax();
    }

    Move * bestMove = findNextBestMove();
    if(bestMove == NULL){
        return NULL;
    }
    boardState->doMove(bestMove, mySide);
    testBoard->doMove(bestMove, mySide);
    return bestMove;
}

/**
 * Represents the simple (naive) heuristic of simply returning the next
 * possible move that garners the highest score out of all next possible
 * moves. This is the greedy approach to making moves.
**/
Move *Player::findNextBestMove()
{
    Move * tempMove = new Move(-1, -1);
    Move * bestMove = new Move(-1, -1);
    int bestScore = -1000;
    for(int i = 0; i < 8; i++){
        for(int j = 0; j < 8; j++){
            tempMove->setX(i);
            tempMove->setY(j);
            if(boardState->checkMove(tempMove, mySide)){
                int tempScore = calculateScore(tempMove, testBoard->copy(), mySide);
                if(tempScore > bestScore){
                    bestMove->setX(i);
                    bestMove->setY(j);
                    bestScore = tempScore;
                }
            }
        }
    }
    delete tempMove;
    if(bestMove->getX() == -1 && bestMove->getY() == -1){
        return NULL;
    }
    return bestMove;
}

/**
 * Calculates the score according to the heuristic:
 * <number of my side> - <number of opponent's side>. This is done by
 * (temporarily) fiddling with 'testBoard' (and then resetting it).
 * A multiplier of 3 is applied to scores involving corner moves
 * while a multiplier of -3 is applied to scores involving next-to-corner
 * moves (since these leave the player very vulnerable to flips by the
 * opponent.
**/
int Player::calculateScore(Move *possibleMove, Board *tempBoard, Side side)
{
    tempBoard->doMove(possibleMove, side);
    int score;
    if(side == WHITE){
        score = tempBoard->countWhite() - tempBoard->countBlack();
    }
    else{
        score = tempBoard->countBlack() - tempBoard->countWhite();
    }
    int x = possibleMove->getX();
    int y = possibleMove->getY();
    // corner multiplier:
    if( (x == 0 && y == 0) || (x == 7 && y == 0) || 
        (x == 0 && y == 7) || (x == 7 && x == 7) ){
        score *= 3;
    }
    // adjacent-to-corner multiplier:
    else if( (abs(x - 0) <= 1 && abs(y - 0) <= 1) ||
             (abs(x - 7) <= 1 && abs(y - 0) <= 1) ||
             (abs(x - 0) <= 1 && abs(y - 7) <= 1) ||
             (abs(x - 7) <= 1 && abs(y - 7) <= 1) ){
        score *= -3;
    }
    else if( x == 0 || x == 7 || y == 0 || y == 7){
        score *= -1;
    }
    delete tempBoard;
//    testBoard = boardState->copy();
    return score;
}
